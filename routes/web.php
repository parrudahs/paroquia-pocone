<?php

Route::get('login', 'Auth\LoginController@getLogin')->name('login');
Route::post('admin', 'Auth\LoginController@postLogin')->name('login.post');

Route::get('/', 'HomeController@index')->name('site.inicio');

Route::get('liturgia/{dia}/{mes}/{ano}', 'lirtugiaController@index')->name('api.liturgia');

Route::get('liturgia-diaria/{dia}/{mes}/{ano}', 'lirtugiaController@lirtugiaPage')->name('site.lirtugia');

Route::get('pedido-oracao', 'PedidoOracaoController@create')->name('pedido.oracao.index');
Route::post('pedido-oracao', 'PedidoOracaoController@store')->name('pedido.oracao.store');

Route::get('velas', 'PedidoOracaoController@acendaVela')->name('pedido.velas.index');
Route::post('velas', 'PedidoOracaoController@storeVela')->name('pedido.velas.store');


Route::get('pagina/{titulo}/{id}', 'PaginaController@show')->name('site.pagina.show')->where("id", "[0-9]+");

Route::get('noticia/{titulo}/{id}', 'PaginaController@show')->name('site.noticia.show')->where("id", "[0-9]+");

Route::get('noticias', 'NoticiaController@getAllNoticias')->name('site.noticia.all');

Route::get('artigos', 'NoticiaController@getAllArtigos')->name('site.artigo.all');

Route::get('oracoes', 'NoticiaController@getAllOracoes')->name('site.oracao.all');

Route::get('galerias-fotos', 'GaleriaController@getAllGalerias')->name('site.galerias.all');

Route::get('comunidades', 'PaginaController@getAllComunidades')->name('site.comunidade.all');

Route::get('galeria-fotos/{titulo}/{id}', 'GaleriaController@getShow')->name('site.galeria.show');


/**
 * ADMINISTRATIVO
 */
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {

	Route::get('deslogar', 'Auth\LoginController@logout')->name('login.deslogar');

	/** Categorias **/
	Route::resource('categorias', 'CategoriaController');

	/** Noticias **/
	Route::resource('noticias', 'NoticiaController');

	/** Paginas **/
	Route::resource('paginas', 'PaginaController');

	/** Galeria **/
	Route::resource('galerias', 'GaleriaController');

	/*** Lirtugia *****/
	Route::resource('lirtugia', 'lirtugiaController');

	Route::get('fotos/{id}', 'GaleriaController@getFotos')->name('fotos.index');
	Route::post('fotos/{id}', 'GaleriaController@postFotos')->name('fotos.store');
	Route::delete('fotos/{id}', 'GaleriaController@deleteFoto')->name('fotos.delete');

	/** Pedidos Oração **/
	Route::resource('pedidos-oracao', 'PedidoOracaoController');

	Route::resource('usuarios', 'UsuarioController');

	Route::get('velas','PedidoOracaoController@getAllVelasAdmin')->name('admin.velas.index');
	Route::get('velas/{id}','PedidoOracaoController@getShowVelasAdmin')->name('admin.velas.show');
	Route::get('velas/{status}/{id}','PedidoOracaoController@getMudarStatus')->name('admin.velas.status');
	Route::delete('velas/{id}','PedidoOracaoController@velaDestroy')->name('admin.velas.destroy');

	Route::get('/', function () {
	    return view('admin/pages/index');
	})->name('home');

});
	
