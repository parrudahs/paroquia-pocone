<?php

namespace Lirtugia\Dominio;

use Lirtugia\Util\HTMLUtils;

/**
 * Algo que pode ser lido
 */
class Leitura {
    private $titulo = "";
    private $texto = "";

    public function __construct($titulo, $texto) {
        $this->titulo = $titulo;
        $this->texto = $texto;
    }

    public function getTitulo() {
        return $this->titulo;
    }

    public function getTexto() {
        return $this->texto;
    }

    public function toArray() {
        return array(
            'titulo'=> HTMLUtils::removeBreak($this->titulo->nodeValue),
            'texto' => HTMLUtils::DOMinnerHTML($this->texto)
        );
    }
}