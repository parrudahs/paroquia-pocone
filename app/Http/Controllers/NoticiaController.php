<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Noticia;
use Image;

class NoticiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::where('tipo', 1)->get();
        return view('admin/pages/noticias/list', compact('noticias'));
    }


    /** Page de todas as Noticias **/
    public function getAllNoticias()
    {
        $noticias = Noticia::where('tipo', 1)->where('ativo', 1)->whereNotIn('categoria_id', [8, 10])->paginate(5);
        return view('site.pages.noticias', compact('noticias'));
    }

    /** Page de todos os artigos **/
    public function getAllArtigos()
    {
        $artigos = Noticia::where('tipo', 1)->where('ativo', 1)->where('categoria_id', 8)->paginate(6);
        return view('site.pages.artigos', compact('artigos'));
    }

    /** Page de todas as oraçoes **/
    public function getAllOracoes()
    {
        $oracoes = Noticia::where('tipo', 1)->where('ativo', 1)->where('categoria_id', 10)->paginate(6);
        return view('site.pages.oracoes', compact('oracoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::whereNotIn('id', [2,4,3])->get();
        return view('admin/pages/noticias/create', compact(['categorias']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|unique:noticias,titulo',
            'conteudo' => 'required',
            'categoria_id' => 'required',
            'imagem_capa' => 'required|image:jpeg,png,bmp'
        ]);

        $image = $request->file('imagem_capa');
        
        $filename  = time().'.'.$image->getClientOriginalExtension();
        
        $pathGrande = public_path('uploads/noticias/grande/'.$filename);
        $pathPequena = public_path('uploads/noticias/pequena/'.$filename);


        $request['capa'] = $filename; 

        $request['resumo'] = substr($request->get('conteudo'), 0, strrpos(substr($request->get('conteudo'), 0, 200), ' ')) . '...';

        if(Noticia::create($request->all())) {

            Image::make($image->getRealPath())->resize(640, 360, 
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save($pathGrande);
                            
            Image::make($image->getRealPath())->resize(300, 100,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save($pathPequena);     

            $request->session()->flash('success', 'A noticia foi adicionada com sucesso');
            return redirect()->route('noticias.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao adicionar a noticia!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = Categoria::whereNotIn('id', [2,4,3])->get();
        $noticia = Noticia::find($id);
        return view('admin/pages/noticias/edit', compact('categorias', 'noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titulo' => 'required',
            'conteudo' => 'required',
            'categoria_id' => 'required',
            'imagem_capa' => 'image:jpeg,png,bmp'
        ]);

        $noticia = Noticia::find($id);

        if($request->hasFile('imagem_capa')){
            $image = $request->file('imagem_capa');
            $filename  = time().'.'.$image->getClientOriginalExtension();
        
            $pathGrande = public_path('uploads/noticias/grande/'.$filename);
            $pathPequena = public_path('uploads/noticias/pequena/'.$filename);
            $request['capa'] = $filename; 

            unlink(public_path('uploads/noticias/grande/').$noticia->capa);
            unlink(public_path('uploads/noticias/pequena/').$noticia->capa);

            Image::make($image->getRealPath())->resize(640, 360, 
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save($pathGrande);
                            
            Image::make($image->getRealPath())->resize(300, 100,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save($pathPequena); 
        }
             
        $request['resumo'] = substr($request->get('conteudo'), 0, strrpos(substr($request->get('conteudo'), 0, 200), ' ')) . '...';

        if($noticia->update($request->all())) {
            $request->session()->flash('success', 'A noticia foi atualizada com sucesso');
            return redirect()->route('noticias.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao atualizar a noticia!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Noticia::find($id);        
        if($delete->delete()){

            unlink(public_path('uploads/noticias/grande/').$delete->capa);
            unlink(public_path('uploads/noticias/pequena/').$delete->capa);

            session()->flash('success', 'A noticia foi removida com sucesso');
            return redirect()->route('noticias.index');
        }
        return redirect()->back()->withInput()->withErrors('Erro ao remover a noticia!');
    }
}
