<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * 
     */
    public function getLogin() 
    {
        return view('admin/login');
    }

    /**
     * 
     */
    public function postLogin(Request $request) 
    {
        $this->validate($request, [
            'email' => 'required|email',
            'senha' => 'required',
        ]);

        $credentials = [
            'email' => $request->email,
            'password' => $request->senha
        ];

        if(Auth::attempt($credentials)){
            return redirect()->intended('admin');
        }else {
            return redirect()->back()->withInput()->withErrors('E-mail ou senha invalida');
        }
    }

    /**
     * 
     */
    public function logout() 
    {
        \Auth::logout();
        return redirect()->intended('/login');
    }    
}
