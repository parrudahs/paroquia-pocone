<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use App\Models\Noticia;
use Image;

class PaginaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticias = Noticia::where('tipo', 2)->get();
        return view('admin/pages/paginas/list', compact('noticias'));
    }

    /** Pagina com todas as comunidades **/
    public function getAllComunidades()
    {
        $comunidades = Noticia::where('tipo', 2)->where('ativo', 1)->where('categoria_id', 2)->paginate(6);
        return view('site.pages.comunidades', compact('comunidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::whereIn('id', [2,4,3])->get();
        return view('admin/pages/paginas/create', compact(['categorias']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|unique:noticias,titulo',
            'conteudo' => 'required',
            'categoria_id' => 'required',
        ]);

        $request['resumo'] = substr($request->get('conteudo'), 0, strrpos(substr($request->get('conteudo'), 0, 200), ' ')) . '...';
        $request['capa']  = '';
 
        if(Noticia::create($request->all())) {
            $request->session()->flash('success', 'A pagina foi adicionada com sucesso');
            return redirect()->route('paginas.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao adicionar a pagina!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($titulo, $id)
    {
        $pagina = Noticia::find($id);
        return view('site.pages.pagina', compact('pagina'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = Categoria::whereIn('id', [2,4,3])->get();
        $noticia = Noticia::find($id);
        return view('admin/pages/paginas/edit', compact('categorias', 'noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titulo' => 'required',
            'conteudo' => 'required',
            'categoria_id' => 'required',
        ]);

        $request['resumo'] = substr($request->get('conteudo'), 0, strrpos(substr($request->get('conteudo'), 0, 200), ' ')) . '...';

        $noticia = Noticia::find($id);

        if($noticia->update($request->all())) {
            $request->session()->flash('success', 'A pagina foi atualizada com sucesso');
            return redirect()->route('paginas.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao atualizar a pagina!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Noticia::find($id);        
        if($delete->delete()){
            session()->flash('success', 'A pagina foi removida com sucesso');
            return redirect()->route('paginas.index');
        }
        return redirect()->back()->withInput()->withErrors('Erro ao remover a pagina!');
    }
}
