<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('admin/pages/usuarios/list', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/usuarios/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|email|unique:users',
            'senha' => 'required|min:6',
            're-senha' => 'required|same:senha',
            'nivel' => 'required',
        ]);

        if(User::create($request->all())) {
            $request->session()->flash('success', 'O usuário foi adicionado com sucesso');
            return redirect()->route('usuarios.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao adicionar usuário!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);
        return view('admin/pages/usuarios/edit', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|email',
            'senha' => 'required|min:6',
            're-senha' => 'required|same:senha',
            'nivel' => 'required',
        ]);

        $usuario = User::find($id);

        if($usuario->update($request->all())) {
            $request->session()->flash('success', 'O usuário foi atualizada com sucesso!');
            return redirect()->route('usuarios.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao atualizar a usuário!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(User::find($id)->delete()){
            session()->flash('success', 'O usuário foi removido com sucesso!');
            return redirect()->route('usuarios.index');
        }

        return redirect()->back()->withInput()->withErrors('Erro ao remover a Usuário!');;
    }
}
