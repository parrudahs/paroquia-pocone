<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;

class CategoriaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();
        return view('admin/pages/categorias/list', compact(['categorias']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/categorias/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|unique:categorias,nome',
        ]);

        if(Categoria::create($request->all())) {
            
            $request->session()->flash('success', 'A Categoria foi adicionada com sucesso');

            return redirect()->route('categorias.index');

        }
        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao adicionar a categoria!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categoria = Categoria::find($id);
        return view('admin/pages/categorias/edit', compact(['categoria']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required|unique:categorias,nome',
        ]);

        if(Categoria::find($id)->update($request->all())) {            
            $request->session()->flash('success', 'A Categoria foi atualizada com sucesso');
            return redirect()->route('categorias.index');
        }
        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao atualizar a categoria!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Categoria::find($id)->delete()){
            session()->flash('success', 'A Categoria foi removida com sucesso');
            return redirect()->route('categorias.index');
        }
        return redirect()->back()->withInput()->withErrors('Erro ao remover a categoria!');        
        
    }
}
