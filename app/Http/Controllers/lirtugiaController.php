<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lirtugia\Dominio\LiturgiaDiaria;

class lirtugiaController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($dia, $mes, $ano)
    {
        $liturgia = new LiturgiaDiaria($dia, $mes, $ano);
        $leituras = $liturgia->toArray();
        return $leituras;
    }

    public function lirtugiaPage($dia, $mes, $ano)
    {
        $liturgia = new LiturgiaDiaria($dia, $mes, $ano);
        $leituras = $liturgia->toArray();
        //dd($leituras);
        return view('site.pages.lirtugia', compact('leituras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
