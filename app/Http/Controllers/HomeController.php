<?php

namespace App\Http\Controllers;

use App\Models\Noticia;
use Illuminate\Http\Request;
use App\Models\Galeria;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $noticiaDestaque = Noticia::where('tipo', 1)
                    ->where('ativo', 1)
                    ->whereNotIn('categoria_id', [8])
                    ->orderBy('created_at', 'desc')
                    ->limit(3)
                    ->get();

        $noticiaSecundaria = Noticia::where('tipo', 1)
                    ->where('ativo', 1)
                    ->whereNotIn('categoria_id', [8])
                    ->orderBy('created_at', 'desc')
                    ->offset(3)
                    ->limit(3)
                    ->get();

        $noticiaArtigos = Noticia::where('tipo', 1)
                    ->where('ativo', 1)
                    ->where('categoria_id', 8)
                    ->orderBy('created_at', 'desc')
                    ->limit(2)
                    ->get();

        $galeriaFotos = Galeria::where('ativo', 1)
                        ->orderBy('created_at', 'desc')
                        ->limit(2)
                        ->get();
        $galeriaSecun = Galeria::where('ativo', 1)
                        ->orderBy('created_at', 'desc')
                        ->offset(2)
                        ->limit(1)
                        ->get();
                    
        return view('site.pages.home', compact('noticiaDestaque', 'noticiaSecundaria', 'galeriaFotos', 'galeriaSecun', 'noticiaArtigos'));
    }
}
