<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PedidoOracao;
use App\Models\Vela;

class PedidoOracaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedidos = PedidoOracao::all();
        return view('admin/pages/pedidooracao/list', compact('pedidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('site.pages.pedidooracao');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|email',
            'mensagem' => 'required|min:10',
            'telefone' => 'required|numeric'
        ]);

        if(PedidoOracao::create($request->all())) {
            
            $request->session()->flash('success', 'Seu Pedido de oração foi enviado com sucesso');

            return redirect()->route('pedido.oracao.index');

        }
        return redirect()->back()->withInput()->withErrors('Erro ao adicionar a categoria!');
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pedido = PedidoOracao::find($id);

        $dados = [
            'status' => 1
        ];
        $pedido->update($dados);

        return view('admin/pages/pedidooracao/show', compact('pedido'));
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(PedidoOracao::find($id)->delete()){
            session()->flash('success', 'O pedido de oração foi removida com sucesso');
            return redirect()->route('pedidos-oracao.index');
        }
        return redirect()->back()->withInput()->withErrors('Erro ao remover o pedido de oração!');
    }

    /**
     * VELAS ACENDENDO
     */


    /**
     * 
     */
    public function getAllVelasAdmin()
    {
        $velas = Vela::all();
        return view('admin/pages/vela/list', compact('velas'));
    }

    /**
     * 
     */
    public function getShowVelasAdmin($id)
    {
        $vela = Vela::find($id);
        return view('admin/pages/vela/show', compact('vela'));
    }

    /**
     * 
     */
    public function getMudarStatus($status, $id)
    {
        $dados['status'] = 1;

        $vela = Vela::find($id)->update($dados);
        session()->flash('success', 'A vela foi acesa! Agora ela está disponivel no site.');
        return redirect()->route('admin.velas.index');
    }

    /**
     * 
     */
    public function acendaVela()
    {
        $velas = Vela::where('status', 1)->paginate(9);
        return view('site.pages.acendavela', compact('velas'));
    }

    /**
     * 
     */
    public function storeVela(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|email',
            'intencao' => 'required|min:10',
        ]);

        if($request['permitir'] == null)
            $request['permitir'] = 1;
        else
            $request['permitir'] = 0;

        if(Vela::create($request->all())) {
            
            $request->session()->flash('success', 'Sua vela foi acesa! Ela estará disponível em nosso site assim que analisarmos seus dados.');

            return redirect()->route('pedido.velas.index');

        }
        return redirect()->back()->withInput()->withErrors('Erro ao adicionar!');
    }

    /**
     * 
     */
    public function velaDestroy($id)
    {
        if(Vela::find($id)->delete()){
            session()->flash('success', 'A vela foi removida com sucesso');
            return redirect()->route('admin.velas.index');
        }
        return redirect()->back()->withInput()->withErrors('Erro ao remover a vela!');
    }
}
