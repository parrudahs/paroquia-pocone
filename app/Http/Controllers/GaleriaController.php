<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Galeria;
use App\Models\Foto;
use Image;

class GaleriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galerias = Galeria::all();
        return view('admin/pages/galerias/list', compact('galerias'));
    }


    /** Page de todos as galerias de fotos **/
    public function getAllGalerias()
    {
        $galerias = Galeria::where('ativo', 1)->paginate(6);
        return view('site.pages.galeriasFotos', compact('galerias'));
    }

    public function getShow($titulo, $id)
    {
         $galeria = Galeria::find($id);
         return view('site.pages.galeria', compact('galeria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/pages/galerias/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|unique:galerias,nome',
            'descricao' => 'required',
            'ativo' => 'required'
        ]);

        if(Galeria::create($request->all())) {
            $request->session()->flash('success', 'A galeria foi adicionada com sucesso');
            return redirect()->route('galerias.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao adicionar a noticia!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeria = Galeria::find($id);
        return view('admin/pages/galerias/edit', compact('galeria'));
    }

    /**
     * Todas as fotos 
     */
    public function getFotos($id) 
    {
        $galeria = Galeria::find($id);
        $fotos =  Foto::where('galeria_id', $id)->orderBy('created_at', 'desc')->get();
        return view('admin/pages/fotos/list', compact('fotos', 'galeria'));
    }


    /**
     * 
     */
    public function postFotos(Request $request, $id) 
    {
        $image = $request->file('file');
        $filename  = time().'-'.uniqid().'.'.$image->getClientOriginalExtension();        
        $pathGrande = public_path('uploads/galerias/grande/'.$filename);
        $pathPequena = public_path('uploads/galerias/pequena/'.$filename);
        
        if(Image::make($image->getRealPath())->resize(600, 600,
            function ($constraint) {
                $constraint->aspectRatio();
            }
        )->save($pathGrande)) {
            Image::make($image->getRealPath())->resize(250, 250, 
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save($pathPequena);

            $request['galeria_id'] = $id;
            $request['nome'] = $filename;

            Foto::create($request->all());
        }
        $request->session()->flash('success', 'As fotos foram adicionadas com sucesso!');
        return response()->json('sucesso', 200);       
    }

    /**
     * 
     */
    public function deleteFoto($id)
    {
        $delete = Foto::find($id);        
        if($delete->delete()){
            unlink(public_path('uploads/galerias/grande/').$delete->nome);
            unlink(public_path('uploads/galerias/pequena/').$delete->nome);

            session()->flash('success', 'A foto foi removida com sucesso');
            return redirect()->back();
        }
        return redirect()->back()->withInput()->withErrors('Erro ao remover a foto!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required|unique:galerias,nome,'.$id,
            'descricao' => 'required',
            'ativo' => 'required'
        ]);

        if(Galeria::find($id)->update($request->all())) {
            $request->session()->flash('success', 'A galeria foi atualizada com sucesso');
            return redirect()->route('galerias.index');
        }

        return redirect()->back()
            ->withInput()
            ->withErrors('Erro ao atualizar a galeria!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
