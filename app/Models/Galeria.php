<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $fillable = [
    	'id',
        'nome',
        'descricao',
        'ativo',
        'user_id'
    ];


    public function fotos() 
    {
    	return $this->hasMany(Foto::class);
    }

}
