<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vela extends Model
{
    protected $table = 'velas';

    protected $fillable = [
		'id',
		'nome',
		'email',
		'permitir',
		'intencao',
		'status'
	];
}
