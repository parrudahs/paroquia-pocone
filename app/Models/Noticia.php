<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model
{
    protected $fillable = [
    	 'titulo','resumo', 'capa', 'conteudo','ativo','tipo','user_id','categoria_id'
    ];

    public function categoria() 
    {
    	return $this->belongsTo(Categoria::class);
    }
}
