<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PedidoOracao extends Model
{
	protected $table = 'pedido_oracoes';

	protected $fillable = [
		'id',
		'nome',
		'email',
		'telefone',
		'mensagem',
		'status'
	];
}
