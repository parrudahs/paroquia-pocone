<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $fillable = [
    	'nome',
    	'galeria_id'
    ];


    public function galeria()
    {
    	return $this->belongsTo(Galeria::class);
    }
}
