<aside class="main-sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="{{ route('home') }}"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
        <li><a href="{{ route('noticias.index') }}"><i class="fa fa-newspaper-o"></i> <span>Noticias</span></a></li>
        <li><a href="{{ route('paginas.index') }}"><i class="fa  fa-file-o"></i> <span>Paginas</span></a></li>
        <li><a href="{{ route('galerias.index') }}"><i class="fa fa-file-photo-o"></i><span>Galerias de Fotos</span></a></li>
        <li><a href="{{ route('categorias.index') }}"><i class="fa fa-link"></i><span>Categoria</span></a></li>
        <li><a href="{{ route('pedidos-oracao.index') }}"><i class="fa fa-commenting-o"></i> <span>Pedidos de Oração</span>
            @if(DB::table('pedido_oracoes')->where('status', 0)->count() > 0)
            <span class="pull-right-container">
              <small class="label pull-right bg-blue">{{ DB::table('pedido_oracoes')->where('status', 0)->count() }}</small>
            </span>
            @endif
        </a> 
        </li>
        <li><a href="{{ route('admin.velas.index') }}"><i class="fa fa-fire"></i> <span>Sistema de Velas</span>
           @if(DB::table('velas')->where('status', 0)->count() > 0)
            <span class="pull-right-container">
              <small class="label pull-right bg-blue">{{ DB::table('velas')->where('status', 0)->count() }}</small>
            </span>
            @endif
        </a></li>      
        <li><a href="{{ route('usuarios.index') }}"><i class="fa fa-users"></i> <span>Usuarios</span></a></li>  
        <li><a href="#"><i class="fa fa-support"></i> <span>Suporte</span></a></li> 
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>