@extends('admin.template')

@section('css')
  <link rel="stylesheet" href="{{ asset('js/plugins/select2/select2.min.css') }}">
@endsection

@section('title', 'Usuários')
@section('sub-title', 'Novo')

@section('rotas')
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('usuarios.index') }}"><i class="fa fa-users"></i> Usuários</a></li>
    <li class="active">Adiciona um novo usuário</li>
@endsection

@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
    @include('admin.parts.errors')
      <!-- /.box-header -->
      <div class="box-body pad">
        <form action="{{ route('usuarios.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" name="nome" placeholder="Nome">
          </div>  
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" placeholder="E-mail">
          </div>  
          <div class="form-group">
            <label>Senha</label>
            <input type="password" class="form-control" name="senha" placeholder="Senha">
          </div>  

          <div class="form-group">
            <label>Repita a senha</label>
            <input type="password" class="form-control" name="re-senha" placeholder="Repita a senha">
          </div>  

          <div class="form-group">              
            <label>Nivel</label>
            <select name="nivel" class="form-control select2" style="width: 100%;">
              <option value="1" selected="selected">Admin</option>
              <option value="2">Editor</option>
            </select>
          </div>    

           <div class="form-group">
            <input type="submit" name="cadastrar" value="Cadastrar Usuário" class="btn btn-success">
            <input type="reset" name="limpar" value="Limpar campos" class="btn btn-primary">
            <a href="{{ route('usuarios.index') }}" title="Voltar" class="btn btn-default">Voltar</a>
          </div>

        </form>              
      </div>
    </div>
    <!-- /.box -->
<!-- ./row -->
@endsection


@section('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
$(".select2").select2();

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
  });
</script>
@endsection

</body>
</html>
