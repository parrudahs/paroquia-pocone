@extends('admin.template')

@section('css')
  <link rel="stylesheet" href="{{ asset('js/plugins/select2/select2.min.css') }}">
@endsection

@section('title', 'Usuário')
@section('sub-title', 'Editar')

@section('rotas')
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('usuarios.index') }}"><i class="fa fa-users"></i> Usuários</a></li>
    <li class="active">Editar usuário</li>
@endsection

@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
    @include('admin.parts.errors')
      <!-- /.box-header -->
      <div class="box-body pad">
        <form action="{{ route('usuarios.update', $usuario->id) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
          <input name="_method" type="hidden" value="PUT">
           <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control"  value="{{ $usuario->nome }}" name="nome" placeholder="Nome">
          </div>  
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" value="{{ $usuario->email }}" name="email" placeholder="E-mail">
          </div>  
          <div class="form-group">
            <label>Senha</label>
            <input type="password" class="form-control" name="senha" placeholder="Senha">
          </div>  
          <div class="form-group">
            <label>Repita a senha</label>
            <input type="password" class="form-control" name="re-senha" placeholder="Repita a senha">
          </div>  

          <div class="form-group">              
            <label>Nivel</label>
             <select name="nivel" class="form-control select2" style="width: 100%;">
              <option value="1" 
              @if($usuario->nivel == 1) 
                selected="selected" 
              @endif >Admin</option>
              <option value="2" 
              @if($usuario->nivel == 2) 
                selected="selected" 
              @endif
              >Editor</option>
            </select>
          </div>    

           <div class="form-group">
            <input type="submit" name="cadastrar" value="Editar Usuário" class="btn btn-success">
           <a href="{{ route('usuarios.destroy', $usuario->id) }}" title="Remover usuário" class="btn btn-danger"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Você tem certeza que deseja remover esse usuário?">Remover usuário</a>
            <a href="{{ route('usuarios.index') }}" title="Voltar" class="btn btn-default">Voltar</a>
          </div>

        </form>              
      </div>
    </div>
    <!-- /.box -->
<!-- ./row -->
@endsection


@section('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
$(".select2").select2();

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
  });
</script>
@endsection

</body>
</html>
