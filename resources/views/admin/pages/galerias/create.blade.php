@extends('admin.template')

@section('css')

@endsection

@section('title', 'Galeria')
@section('sub-title', 'Nova')

@section('rotas')
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('galerias.index') }}"><i class="fa fa-file-photo-o"></i> Galerias</a></li>
    <li class="active">Nova Galeria</li>
@endsection

@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <!-- /.box-header -->
      <div class="box-body pad">
        @include('admin.parts.errors')
        <form action="{{ route('galerias.store') }}"  method="post"  enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Nome</label>
            <input type="text" name="nome" class="form-control" value="{{ old('nome') }}" placeholder="Nome da Galeria">
            <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id }}">
          </div>
          <div class="form-group">
            <label>Descrição</label>
            <textarea id="editor1" name="descricao" rows="5" cols="80"></textarea>
          </div>
          <div class="form-group">              
            <label>Status</label>
            <select name="ativo" class="form-control select2" style="width: 100%;">
              <option value="1" selected="selected">Publicar</option>
              <option value="0">Rascunho</option>
            </select>
          </div>  

          <div class="form-group">
            <input type="submit" name="cadastrar" value="Cadastrar Galeria" class="btn btn-success">
            <input type="reset" name="limpar" value="Limpar campos" class="btn btn-primary">
            <a href="{{ route('galerias.index') }}" title="Voltar" class="btn btn-default">Voltar</a>
          </div>
        </form>
              
      </div>
    </div>
    <!-- /.box -->
<!-- ./row -->
@endsection


@section('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<script>
  $(function () {
    CKEDITOR.replace('editor1');
  });
</script>
@endsection

</body>
</html>
