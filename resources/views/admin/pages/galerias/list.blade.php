@extends('admin.template')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('title', 'Galerias')
@section('sub-title', 'Todas')

@section('rotas')
  <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Galerias</li>
@endsection

@section('conteudo')
  <div class="box">
      <div class="box-header">
        <a href="{{ route('galerias.create') }}" class="btn btn-block btn-primary btn-lg">
          Adicionar uma nova galeria
        </a>        
      </div>
      
        <!-- /.box-header -->
        <div class="box-body">
        @include('admin.parts.success')
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Status</th>
              <th>Qtd Fotos</th>
              <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($galerias as $c)
            <tr>
              <td>{{ $c->id }}</td>
              <td>{{ $c->nome }}</td>
              <td>@if($c->ativo == 1) Publicado @else Rascunho @endif</td>
              <td>{{ count($c->fotos) }}</td>
              <td class="text-center">
                  <a href="{{ route('fotos.index', $c->id) }}" title="Adicionar/Remover Fotos" class="btn-sm btn-success"><i class="fa fa-plus"></i></a>

                  <a href="{{ route('galerias.edit', $c->id) }}" title="Editar galeria" class="btn-sm btn-primary"><i class="fa fa-edit"></i></a>       

                  <a href="{{ route('galerias.destroy', $c->id) }}" title="Remover Galeria" class="btn-sm btn-danger"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Você tem certeza que deseja remover essa galeria?">
                    <i class="fa fa-remove"></i>
                  </a>
              </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Status</th>
              <th>Qtd Fotos</th>
              <th>Ações</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
      <!-- /.row -->
@endsection
      
@section('js')

<!-- DataTables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->

<script>
  $(function () {
    $("#example1").DataTable( {
      "language" :{
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
      },
    });    
  });
</script>

@endsection
