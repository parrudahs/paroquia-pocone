@extends('admin.template')

@section('css')
  <link rel="stylesheet" href="{{ asset('js/plugins/select2/select2.min.css') }}">
@endsection

@section('title', 'Noticias')
@section('sub-title', 'Nova')

@section('rotas')
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('noticias.index') }}"><i class="fa fa-newspaper-o"></i>Noticias</a></li>
    <li class="active">Nova Noticia</li>
@endsection

@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
    @include('admin.parts.errors')
      <!-- /.box-header -->
      <div class="box-body pad">
        <form action="{{ route('noticias.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="form-group">
            <label>Titulo</label>
            <input type="text" class="form-control" name="titulo" placeholder="">
            <input type="hidden" class="form-control" name="tipo" value="1">
            <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id }}">
          </div>  
          <div class="form-group">
            <label>Escolha uma capa</label>
            <input type="file" class="form-control" name="imagem_capa">
          </div>         
          <div class="form-group">              
            <label>Categoria</label>
            <select name="categoria_id" class="form-control select2" style="width: 100%;">
              <option selected="selected">Selecione a Categoria </option>
              @foreach($categorias as $c)              
                <option value="{{ $c->id }}">{{ $c->nome }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label>Conteudo</label>
            <textarea id="editor1" name="conteudo" rows="10" cols="80"></textarea>
          </div>

          <div class="form-group">              
            <label>Status</label>
            <select name="ativo" class="form-control select2" style="width: 100%;">
              <option value="1" selected="selected">Publicar</option>
              <option value="0">Rascunho</option>
            </select>
          </div>    

           <div class="form-group">
            <input type="submit" name="cadastrar" value="Cadastrar Noticia" class="btn btn-success">
            <input type="reset" name="limpar" value="Limpar campos" class="btn btn-primary">
            <a href="{{ route('noticias.index') }}" title="Voltar" class="btn btn-default">Voltar</a>
          </div>

        </form>              
      </div>
    </div>
    <!-- /.box -->
<!-- ./row -->
@endsection


@section('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
$(".select2").select2();

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
  });
</script>
@endsection

</body>
</html>
