@extends('admin.template')

@section('css')

@endsection

@section('title', 'Categoria')
@section('sub-title', 'Atualizar')

@section('rotas')
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('categorias.index') }}"><i class="fa fa-link"></i> Categorias</a></li>
    <li class="active">Editar categoria</li>
@endsection

@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <!-- /.box-header -->
      <div class="box-body pad">
        @include('admin.parts.errors')
        <form action="{{ route('categorias.update', $categoria->id) }}"  method="post">
          {{ csrf_field() }}
          <input name="_method" type="hidden" value="PUT">
          <div class="form-group">
            <label>Nome</label>
            <input type="text" name="nome" class="form-control" value="{{ $categoria->nome }}" placeholder="Nome da categoria">
          </div>

          <div class="form-group">
            <input type="submit" name="Editar" value="Editar Categoria" class="btn btn-success">
            <a href="{{ route('categorias.destroy', $categoria->id) }}" title="Remover categoria" class="btn btn-danger"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Are you sure?">Remover Categoria</a>
            
            <a href="{{ route('categorias.index') }}" title="Voltar" class="btn btn-default">Voltar</a>
          </div>
        </form>
              
      </div>
    </div>
    <!-- /.box -->
<!-- ./row -->
@endsection


@section('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<script>
  $(function () {
    CKEDITOR.replace('editor1');
  });
</script>
@endsection

</body>
</html>
