@extends('admin.template')

@section('css')

@endsection

@section('title', 'Categoria')
@section('sub-title', 'Nova')

@section('rotas')
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('categorias.index') }}"><i class="fa fa-link"></i> Categorias</a></li>
    <li class="active">Nova Categoria</li>
@endsection

@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
      <!-- /.box-header -->
      <div class="box-body pad">
        @include('admin.parts.errors')
        <form action="{{ route('categorias.store') }}"  method="post">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Nome</label>
            <input type="text" name="nome" class="form-control" value="{{ old('nome') }}" placeholder="Nome da categoria">
          </div>

          <div class="form-group">
            <input type="submit" name="cadastrar" value="Cadastrar Categoria" class="btn btn-success">
            <input type="reset" name="limpar" value="Limpar campos" class="btn btn-primary">
            <a href="{{ route('categorias.index') }}" title="Voltar" class="btn btn-default">Voltar</a>
          </div>
        </form>
              
      </div>
    </div>
    <!-- /.box -->
<!-- ./row -->
@endsection


@section('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>

<script>
  $(function () {
    CKEDITOR.replace('editor1');
  });
</script>
@endsection

</body>
</html>
