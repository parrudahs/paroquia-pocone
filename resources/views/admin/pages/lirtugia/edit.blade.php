@extends('admin.template')

@section('css')
  <link rel="stylesheet" href="{{ asset('js/plugins/select2/select2.min.css') }}">
@endsection

@section('title', 'Noticias')
@section('sub-title', 'Atualizar')

@section('rotas')
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ route('noticias.index') }}">Noticias</a></li>
    <li class="active">Nova Noticia</li>
@endsection

@section('conteudo')
<div class="row">
  <div class="col-md-12">
    <div class="box box-info">
    @include('admin.parts.errors')
      <!-- /.box-header -->
      <div class="box-body pad">
        <form action="{{ route('noticias.update', $noticia->id) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
          <input name="_method" type="hidden" value="PUT">
          
          <div class="form-group">
            <label>Titulo</label>
            <input type="text" class="form-control" value="{{ $noticia->titulo }}" name="titulo" placeholder="">
            <input type="hidden" class="form-control" name="tipo" value="1">
            <input type="hidden" class="form-control" name="user_id" value="{{Auth::user()->id }}">
          </div>  
           <div class="form-group">
            <label>Escolha uma capa</label>
            <input type="file" class="form-control" name="imagem_capa">
          </div>         
          <div class="form-group">              
            <label>Categoria</label>
            <select name="categoria_id" class="form-control select2" style="width: 100%;">
              <option selected="selected">Selecione a Categoria </option>
              @foreach($categorias as $c)
                @if( $noticia->categoria_id == $c->id )       
                  <option value="{{ $c->id }}" selected="selected">{{ $c->nome }}</option>
                @else       
                  <option value="{{ $c->id }}">{{ $c->nome }}</option>
                @endif
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label>Conteudo</label>
            <textarea id="editor1"  name="conteudo" rows="10" cols="80">{{ $noticia->conteudo }}</textarea>
          </div>

          <div class="form-group">              
            <label>Status</label>
            <select name="ativo" class="form-control select2" style="width: 100%;">
              <option value="1" 
              @if($noticia->ativo == 1) 
                selected="selected" 
              @endif >Publicar</option>
              <option value="0" 
              @if($noticia->ativo == 0) 
                selected="selected" 
              @endif
              >Rascunho</option>
            </select>
          </div>    

           <div class="form-group">
            <input type="submit" name="cadastrar" value="Editar Noticia" class="btn btn-success">
           <a href="{{ route('categorias.destroy', $noticia->id) }}" title="Remover Noticia" class="btn btn-danger"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Are you sure?">Remover Categoria</a>
            <a href="{{ route('noticias.index') }}" title="Voltar" class="btn btn-default">Voltar</a>
          </div>

        </form>              
      </div>
    </div>
    <!-- /.box -->
<!-- ./row -->
@endsection


@section('js')
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
$(".select2").select2();

<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
  });
</script>
@endsection

</body>
</html>
