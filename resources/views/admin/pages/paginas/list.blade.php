@extends('admin.template')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('title', 'Paginas')
@section('sub-title', 'Todas')

@section('rotas')
  <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
  <li class="active">Paginas</li>
@endsection

@section('conteudo')
   <div class="box">
      <div class="box-header">
        <a href="{{ route('paginas.create') }}" class="btn btn-block btn-primary btn-lg">
          Adicionar uma nova pagina
        </a>        
      </div>      
        <!-- /.box-header -->
        <div class="box-body">
        @include('admin.parts.success')
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nome</th>
                  <th>Categorias</th>
                  <th>Status</th>
                  <th>Data Cadastro</th>
                  <th>Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($noticias as $n)
                  <tr>
                    <td>{{ $n->id }}</td>
                    <td>{{ $n->titulo}}</td>
                    <td>{{ $n->categoria->nome  }}</td>
                    <td>@if($n->ativo == 1) Publicado @else Rascunho @endif</td>
                    <td>{{ $n->created_at->format('d/m/Y') }}</td>
                    <td>
                      <a href="{{ route('paginas.edit', $n->id) }}" title="Editar Pagina" class="btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                      @if($n->categoria_id != 4)
                      <a href="{{ route('paginas.destroy', $n->id) }}" title="Remover Pagina" class="btn-sm btn-danger"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Você tem certeza que deseja remover a noticia?">
                      <i class="fa fa-remove"></i>
                      </a>
                      @endif

                    </td>
                  </tr>    
                @endforeach          
                </tbody>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Categorias</th>
                    <th>Status</th>
                    <th>Data Cadastro</th>
                    <th>Ações</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
@endsection
      
@section('js')

<!-- DataTables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->

<script>
  $(function () {
    $("#example1").DataTable( {
      "language" :{
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
      }
    });    
  });
</script>

@endsection
