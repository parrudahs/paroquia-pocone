@extends('admin.template')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('title', 'Pedidos de Orações')
@section('sub-title', 'Todos')

@section('rotas')
  <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Pedidos de orações</li>
@endsection

@section('conteudo')
  <div class="box">
      <div class="box-header">     
      </div>
      
        <!-- /.box-header -->
        <div class="box-body">
        @include('admin.parts.success')
          <table id="example1" data-order='[[ 0, "desc" ]]' class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Telefone</th>
              <th>Email</th>
              <th>Data</th>
              <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pedidos as $c)
            <tr @if($c->status == 0) class="danger" @endif>
              <td>{{ $c->id }}</td>
              <td>{{ $c->nome }}</td>
              <td>{{ $c->telefone }}</td>
              <td>{{ $c->email }}</td>
              <td>{{ date('d/m/Y', strtotime($c->created_at)) }}</td>
              <td class="text-center">
                  <a href="{{ route('pedidos-oracao.show', $c->id) }}" title="Ver pedido de oração" class="btn-sm btn-default" >
                    <i class="fa fa-eye"></i>
                  </a>
                  <a href="{{ route('pedidos-oracao.destroy', $c->id) }}" title="Remover categoria" class="btn-sm btn-danger"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Você tem certeza que deseja remover esse pedido de oração?">
                    <i class="fa fa-remove"></i>
                  </a>
              </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Telefone</th>
              <th>Email</th>
              <th>Data</th>
              <th>Ações</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
      <!-- /.row -->
@endsection
      
@section('js')

<!-- DataTables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/plugins/fastclick/fastclick.js') }}"></script>

<!-- AdminLTE App -->

<script>
  $(function () {
    $("#example1").DataTable( {
      "language" :{
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "Pesquisar",
        "oPaginate": {
            "sNext": "Próximo",
            "sPrevious": "Anterior",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
      },
    });    
  });
</script>

@endsection
