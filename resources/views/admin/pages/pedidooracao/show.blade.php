@extends('admin.template')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('title', 'Pedidos de Oração')

@section('rotas')
  <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Detalhes do pedido de oração</li>
@endsection

@section('conteudo')
  <div class="box">
      <div class="box-header">
			
      </div>
      
        <!-- /.box-header -->
        <div class="box-body">
        	<p><strong>De:</strong> {{ $pedido->nome }}</p>
        	<p><strong>Telefone:</strong> {{ $pedido->telefone }} </p>
        	<p><strong>E-mail:</strong> {{ $pedido->email }} </p>
        	<p><strong>Mensagem:</strong> {{ $pedido->mensagem }} </p>
          	<a href="{{ route('pedidos-oracao.index') }}" class="btn btn-default"> Voltar </a>
          	<button type="button" class="btn btn-default"><i class="fa fa-print"></i> Imprimir</button>
      <!-- /.box -->
    	</div>
    <!-- /.col -->
  </div>
      <!-- /.row -->
@endsection
      
@section('js')

<!-- DataTables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/plugins/fastclick/fastclick.js') }}"></script>

<!-- AdminLTE App -->
@endsection
