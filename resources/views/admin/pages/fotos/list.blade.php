@extends('admin.template')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
@endsection

@section('title', 'Fotos')
@section('sub-title', '')

@section('rotas')
  <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
  <li><a href="{{ route('galerias.index') }}"><i class="fa fa-file-photo-o"></i> Galerias</a></li>
  <li class="active">Fotos</li>
@endsection

@section('conteudo')
    <div class="box">
      <div class="box-header">
      @if(Session::has('success'))
            <div class="alert alert-success">
              {!! Session::get('success') !!}
            </div>
      @endif
      <h3 class="box-title">Adicionar fotos na Galeria: <strong>{{ $galeria->nome }}</strong></h3>
      <form action="{{ route('fotos.store', $galeria->id) }}" method="post" class="dropzone" id="my-awesome-dropzone">
      {{ csrf_field() }}
      </form>   
      </div>   
    </div>  

    <div class="box"> 
      <div class="box-header">
        <h3 class="box-title">Todas as Fotos da Galeria</h3>
      </div>
        <!-- /.box-header -->
        <div class="box-body">
        <div class="row">
          @foreach($fotos as $f)
          <div class="col-md-3" style="height: 200px;">
            <div class="thumbnail">
              <img class="img-responsive" src="{{ asset('uploads/galerias/pequena') }}/{{ $f->nome }}" alt="Photo">
              <a href="{{ route('fotos.delete', $f->id) }}" title="Remover categoria" class="btn-sm btn-danger btn-block text-center"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Are you sure?">
              <i class="fa fa-remove"></i></a>
            </div>
          </div>
          @endforeach
        </div>
        </div>
      <!-- /.box-body -->
  </div>
    <!-- /.box -->
@endsection
      
@section('js')

<!-- DataTables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('js/dropzone.js') }}"></script>

<script>
  ;(function($)
  {
    'use strict';
    $(document).ready(function()
    {
      Dropzone.options.myAwesomeDropzone = {
        addRemoveLinks: false,
        dictDefaultMessage: "Arraste e solte as fotos aqui!",
        init: function() {
          this.on("queuecomplete", function(file) {  
            location.reload();
          });
        }
      };

    });
  })(window.jQuery);
</script>

@endsection
