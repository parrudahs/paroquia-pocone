@extends('admin.template')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('title', 'Detalhe da Vela')

@section('rotas')
  <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Vela</li>
@endsection

@section('conteudo')
  <div class="box">
      <div class="box-header">
			
      </div>
      
        <!-- /.box-header -->
        <div class="box-body">
        	<p><strong>Nome:</strong> {{ $vela->nome }}</p>
        	<p><strong>E-mail:</strong> {{ $vela->email }} </p>
        	<p><strong>Mensagem:</strong> {{ $vela->intencao }} </p>
          	<a href="{{ route('admin.velas.index') }}" class="btn btn-default"> Voltar </a>
          	<button type="button" class="btn btn-default"><i class="fa fa-print"></i> Imprimir</button>
      <!-- /.box -->
    	</div>
    <!-- /.col -->
  </div>
      <!-- /.row -->
@endsection
      
@section('js')

<!-- DataTables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/plugins/fastclick/fastclick.js') }}"></script>
@endsection
