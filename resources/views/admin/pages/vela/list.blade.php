@extends('admin.template')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap.css') }}">
@endsection

@section('title', 'Velas Acesas')
@section('sub-title', 'Todas')

@section('rotas')
  <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li class="active">Todas as velas</li>
@endsection

@section('conteudo')
  <div class="box">
      <div class="box-header">     
      </div>
      
        <!-- /.box-header -->
        <div class="box-body">
        @include('admin.parts.success')
          <table id="example1" data-order='[[ 0, "desc" ]]' class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Email</th>
              <th>Intenção</th>
              <th>Data</th>
              <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($velas as $c)
            <tr @if($c->status == 0) class="danger" @endif>
              <td>{{ $c->id }}</td>
              <td>{{ $c->nome }}</td>
              <td>{{ $c->email }}</td>
              <td>{{ $c->intencao }}</td>
              <td>{{ date('d/m/Y', strtotime($c->created_at)) }}</td>
              <td class="text-center">
                  <a href="{{ route('admin.velas.status', [$c->status, $c->id]) }}" class="btn-sm btn-success" title="Aprovar pedido para acender vela">
                      <i class="fa fa-check-square-o"></i>
                  </a>
                  <a href="{{ route('admin.velas.show', $c->id) }}" title="ver detalhes da vela" class="btn-sm btn-default" >
                    <i class="fa fa-eye"></i>
                  </a>
                  <a href="{{ route('admin.velas.destroy', $c->id) }}" title="Remover Vela" class="btn-sm btn-danger"  data-method="delete" data-token="{{csrf_token() }}" data-confirm="Você tem certeza que deseja remover essa vela?">
                  <i class="fa fa-remove"></i>
                  </a>
              </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>Email</th>
              <th>Intenção</th>
              <th>Data</th>
              <th>Ações</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
      <!-- /.row -->
@endsection
      
@section('js')

<!-- DataTables -->
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('js/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('js/plugins/fastclick/fastclick.js') }}"></script>
@endsection
