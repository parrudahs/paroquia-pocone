<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>Paroquia Nossa Senhora do Rosário</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
</head>
<body>
	<div class="container">
		<!-- sidebar a esquerda -->
		<div class="col-sm-3 sidebar-esquerda"> 
			<div class="row logo">
				<a href="#"><img src="imagens/nova_logo_marca.png"></a>
				<p class="text-center">Poconé, 08 de março de 2017</p>
			</div>
			<div class="row menu">
				<div class="sidebar-nav">
		      <div class="navbar navbar-default" role="navigation">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <span class="visible-xs navbar-brand">MENU</span>
		        </div>
		        <div class="navbar-collapse collapse sidebar-navbar-collapse">
		          <ul class="nav navbar-nav menu-destaque">
		            <li><a href="#" class="destaque">Pagina Inicial</a></li>
		          </ul>
		          <ul class="nav navbar-nav menu-destaque">
		           	<li><a href="#" class="destaque">Nossa Paroquia</a></li>
		            <li><a href="#">Historia</a></li>
		            <li><a href="#">Comunidades</a></li>
		            <li><a href="#">Paroco</a></li>
		            <li><a href="#">Calendario Paroquial</a></li>
		            <li><a href="#">Como chegar</a></li>
		            <li><a href="#">Horarios de missa</a></li>
		            <li><a href="#">Secretaria Paroquial</a></li>
		          </ul>
		          <ul class="nav navbar-nav menu-destaque">
		           	<li><a href="#" class="destaque">Informações</a></li>
		            <li><a href="#">Noticias</a></li>
		            <li><a href="#">Artigos</a></li>
		            <li><a href="#">Oracoes</a></li>
		            <li><a href="#">Lirtugia Diaria</a></li>
		          </ul>
		          <ul class="nav navbar-nav menu-destaque">
		            <li><a href="#" class="destaque">multimidia</a></li>
		            <li><a href="#">Menu Item 3</a></li>
		            <li><a href="#">Menu Item 4</a></li>
		          </ul>
		        </div><!--/.nav-collapse -->		        
		      </div>
    		</div>
			</div>
		</div>	
		<!-- FIM sidebar a esquerda -->
		<!-- Conteudo -->
		<div class="col-sm-9 all-content">
		<div class="row">
			<div class="col-sm-9 lirtugia-top">
				<h4>Liturgia Diaria: <small> <a href="">1º leitura de domingo </a></small></h4>
			</div>
			<div class="col-sm-3 busca">
				<div class="input-group">
			      <input type="text" class="form-control" placeholder="Buscar">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon glyphicon-search" aria-hidden="true"></span></button>
			      </span>
			    </div><!-- /input-group -->
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9 content">
				<!-- inicio do banner -->
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="imagens/papa.jpg" alt="imagem do banner 1">
				      <div class="carousel-caption">
				      	<a href="">
					      	<small>noticias</small>
					       	<h5>Papa Francisco 1 Lorem ipsulum, testando o titulo.</h5>
				       	</a>
				      </div>
				    </div>
				    <div class="item">
				      <img src="imagens/papa.jpg" alt="imagem do banner 2">
				      <div class="carousel-caption">
				      	<a href="">
					      	<small>noticias</small>
					        <h5>Papa Francisco nomeia 3 novos bispos para o Brasil. </h5>
				        </a>
				      </div>
				    </div>
				  </div>
				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Anterior</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Proximo</span>
				  </a>
				</div>
				<!-- fim do banner -->

				<!-- INICIO das Noticias -->
				<div class="row noticias">
					<div class="col-sm-4 box">
						<a href="">
							<img src="imagens/papa.jpg" class="img-responsive">
						</a>
						<small class="noticia-categoria">noticias</small>
						<a href="">
							<p>Papa Francisco nomeia 3 novos bispos para o Brasil.  </p>
						</a>	
						<small class="noticia-data">10/05/2017</small>				
					</div>
					<div class="col-sm-4 box">
						<a href="">
							<img src="imagens/papa.jpg" class="img-responsive">
						</a>
						<small class="noticia-categoria">noticias</small>
						<a href="">
							<p>Papa Francisco nomeia 3 novos bispos para o Brasil.  </p>
						</a>	
						<small class="noticia-data">10/05/2017</small>							
					</div>
					<div class="col-sm-4 box">
						<a href="">
							<img src="imagens/papa.jpg" class="img-responsive">
						</a>
						<small class="noticia-categoria">noticias</small>
						<a href="">
							<p>Papa Francisco nomeia 3 novos bispos para o Brasil.  </p>
						</a>	
						<small class="noticia-data">10/05/2017</small>						
					</div>
				</div>
				<!-- FIM das Noticias -->
				<!-- 	INICIO DO DIZIMISTA -->
				<div class="col-sm-12 dizimista-velas">
					<div class="col-sm-3 box">
						<a href="#">
							<img src="imagens/dizimista.png" class="img-responsive">
							<p>Seja Dizimista</p>
						</a>
					</div>
					<div class="col-sm-3 box">
						<a href="#">
							<img src="imagens/dizimista.png" class="img-responsive">
							<p>Acenda uma Vela</p>
						</a>
					</div>
					<div class="col-sm-3 box">
						<a href="#">
							<img src="imagens/dizimista.png" class="img-responsive">
							<p>Pedido de Oração</p>
						</a>
					</div>
					<div class="col-sm-3 box">
						<a href="#">
							<img src="imagens/dizimista.png" class="img-responsive">
							<p>Acenda uma Vela</p>
						</a>
					</div>
				</div>
				<!-- FIM do DIZIMISTA -->
				<!-- INICIO do ARTIGO -->
				<div class="row artigo">
					<div class="col-sm-6 box"> 
						<small class="categoria-artigo">artigos</small>
						<a href="">							
							<p>Papa Francisco nomeia 3 novos bispos para o Brasil.</p>		
						</a>		
						<small class="data-artigo">10/05/2017</small>		
					</div>
					<div class="col-sm-6 box"> 
						<small class="categoria-artigo">reflexões</small>
						<a href="">							
							<p>Papa Francisco nomeia 3 novos bispos para o Brasil.</p>		
						</a>		
						<small class="data-artigo">10/05/2017</small>
					</div>
				</div>
				<!-- FIM do ARTIGO -->
				<!-- INICIO GALERIA DE FOTOS -->
				<div class="row galeria">
					<div class="title-galeria">
							<h3>GALERIA DE FOTOS
							<a class="btn btn-primary" role="button">
							  Veja Mais
							</a>
							</h3>
					</div>				
					<div class="col-sm-8">
						<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="media-object" src="imagens/papa.jpg" width="140">
						    </a>
						  </div>
						  <div class="media-body">
						  	<small class="galeria-categoria">noticias</small>
								<a href="#"><p class="media-heading">Papa Francisco nomeia 3 novos bispos para o Brasil.</p></a>
								<small class="data-galeria">10/05/2017</small>
						  </div>
						</div>

						<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="media-object" src="imagens/papa.jpg" width="140">
						    </a>
						  </div>
						  <div class="media-body">
							 	<small class="galeria-categoria">noticias</small>
								<a href="#"><p class="media-heading">Papa Francisco nomeia 3 novos bispos para o Brasil.</p></a>
								<small class="data-galeria">10/05/2017</small>
						  </div>
						</div>
					</div>
					<div class="col-sm-4 box">
						<div class="media-body">
							<a href="">
								<img src="imagens/papa.jpg" class="img-responsive">
							</a>
							<small class="galeria-categoria">noticias</small>
							<a href="#"><p>Papa Francisco nomeia 3 novos bispos para o Brasil.</p></a>
							<small class="data-galeria">10/05/2017</small>	
						</div>
					</div>
				</div>
				<!-- FIM GALERIA DE FOTOS -->
			</div>

			<div class="col-sm-3">
				<div class="sidebar-direita">
					<div class="row">
						<h1>Avisos</h1>
						<p>Na igreja Matriz: Domingos: 7h30min, 9h e 18h Sábados: 17h Sextas-feiras: 16h (Hora Santa Eucarística e bênção do Santíssimo Sacramento, 15h)... </p>
						<a class="btn btn-primary button-avisos" role="button">
							  saiba mais
						</a>
					</div>
					<div class="row">
						<h1>Pastorais, Movimentos e Ministerios</h1>
						<ul class="nav nav-pills nav-stacked">
							<li class="destaque"><a href="#">Página Inicial</a></li>
							<li><a href="#">Action</a></li>
		            <li><a href="#">Another action</a></li>
		            <li><a href="#">Something else here</a></li>
		            <li class="destaque"><a href="#">Nossa Paroquia</a></li>
		            <li><a href="#">Action</a></li>
		            <li><a href="#">Another action</a></li>
		            <li><a href="#">Something else here</a></li>
						</ul>
					</div>
					<div class="row">
						<div class="fb-page"
						  data-href="https://www.facebook.com/imdb" 
						  data-width="340"
						  data-hide-cover="false"
						  data-show-facepile="true">
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
		<!-- Fim Content -->
	<footer class="col-sm-12 rodape">
		<p class="text-center">Todos os direitos reservados a Paroquia</p>
	</footer>	
	</div>

	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js" type="text/javascript" ></script>
</body>
</html>		