@extends('site.template')

@section('css')
    <link href="{{ asset('css/lightslider.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('js')
	<script type="text/javascript" src="{{ asset('js/lightslider.js') }}"></script>
    <script>
         $(document).ready(function() {
           $('#image-gallery').lightSlider({
                gallery:true,
                adaptiveHeight:true,
                item:1,
                keyPress:true,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:false,
                pager:true,
                loop:false,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
        });
    </script>
@endsection

@section('conteudo')
	<div class="page-galeria col-sm-12">
		<h2>{{ $galeria->nome }}</h2>
		<hr>
		<span class="date">{{ date('d/m/Y', strtotime($galeria->created_at)) }}</span>
        <p>{!! $galeria->descricao !!}</p>

        <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
        @foreach($galeria->fotos as $f)
            <li data-thumb="{{ asset('uploads/galerias/pequena/'.$f['nome']) }}" thumb-url="{{ asset('uploads/galerias/pequena/'.$f['nome']) }}"> 
            <img src="{{ asset('uploads/galerias/grande/'.$f['nome']) }}" thumb-url="{{ asset('uploads/galerias/grande/'.$f['nome']) }}" />
             </li>
        @endforeach
        </ul>
	</div>
@endsection
