@extends('site.template')

@section('conteudo')
	<!-- inicio do banner -->
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">
	    @php 
			$i = 1;
	    @endphp
	    @foreach($noticiaDestaque as $n)
		    <div class=" @if( $i <= 1) item active @else item" @endif">
		      <img width="100%" src="{{ asset('uploads/noticias/grande/'.$n->capa) }}" alt="{{ $n->titulo }}">
		      <div class="carousel-caption">
		      	<a href="{{ route('site.noticia.show', [str_slug($n->titulo, '-'), $n->id]) }}">
			      	<small>{{ $n->categoria->nome }}</small>
			        <h5>{{ $n->titulo }}</h5>
		        </a>
		      </div>
		    </div>
		    @php
		    	$i++;
		    @endphp
	    @endforeach
	  	</div>
	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Anterior</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Proximo</span>
	  </a>
	</div>
	<!-- fim do banner -->

	<!-- INICIO das Noticias -->
	<div class="row noticias">
		@foreach($noticiaSecundaria as $n)
		<div class="col-sm-4 col-xs-12 box">
			<div class="col-xs-11">
				<a href="{{ route('site.noticia.show', [str_slug($n->titulo, '-'), $n->id]) }}">
					<img src="{{ asset('uploads/noticias/pequena/'.$n->capa) }}" class="img-responsive col-xs-12">
				</a>
				<small class="noticia-categoria">{{ $n->categoria->nome }}</small>
				<a href="{{ route('site.noticia.show', [str_slug($n->titulo, '-'), $n->id]) }}">
					<p>{{ $n->titulo }}</p>
				</a>	
				<small class="noticia-data">{{ date('d/m/Y', strtotime($n->created_at)) }}</small>		
			</div>			
		</div>
		@endforeach
	</div>
	<!-- FIM das Noticias -->
	<!-- 	INICIO DO DIZIMISTA -->
	<div class="col-sm-12 col-xs-12 dizimista-velas">
		<div class="col-sm-3 col-xs-6 box">
			<a href="{{ route('site.pagina.show', [str_slug('Seja Dizimista', '-'), 16]) }}" >
				<img src="imagens/dizimista.png" class="img-responsive">
				<p>Seja Dizimista</p>
			</a>
		</div>
		<div class="col-sm-3 col-xs-6 box">
			<a href="{{ route('site.comunidade.all') }}">
				<img src="imagens/comunidades.png" class="img-responsive">
				<p>Comunidades</p>
			</a>
		</div>
		<div class="col-sm-3 col-xs-6 box">
			<a href="{{ route('pedido.oracao.index') }}">
				<img src="imagens/oracao.png" class="img-responsive">
				<p>Pedido de Oração</p>
			</a>
		</div>
		<div class="col-sm-3 col-xs-6 box">
			<a href="{{ route('pedido.velas.index') }}">
				<img src="imagens/vela.png" class="img-responsive">
				<p>Acenda uma Vela</p>
			</a>
		</div>
	</div>
	<!-- FIM do DIZIMISTA -->
	<!-- INICIO do ARTIGO -->
	<div class="row artigo">
		<div class="col-xs-12">
			@foreach($noticiaArtigos as $n)
			<div class="col-sm-6 col-xs-12 box"> 
				<small class="categoria-artigo">{{ $n->categoria->nome }}</small>
				<a href="{{ route('site.noticia.show', [str_slug($n->titulo, '-'), $n->id]) }}">							
					<p>{{ $n->titulo }}</p>		
				</a>		
				<small class="data-artigo">{{ date('d/m/Y', strtotime($n->created_at)) }}</small>		
			</div>
			@endforeach
		</div>
	</div>
	<!-- FIM do ARTIGO -->
	<!-- INICIO GALERIA DE FOTOS -->
	<div class="row galeria">
		<div class="title-galeria">
			<h3>GALERIAS DE FOTOS
			<a class="btn btn-primary" role="button" href="{{ route('site.galerias.all') }}">
			  Veja Mais
			</a>
			</h3>
		</div>				
		<div class="col-sm-8 col-xs-12">
			@foreach($galeriaFotos as $g)
			<div class="media ">
			  <div class="media-left">
			    <a href="{{ route('site.galeria.show', [str_slug($g->nome, '-'), $g->id]) }}">
			      <img class="media-object" src="{{ asset('uploads/galerias/pequena/'.$g->fotos[0]->nome) }}" width="140">
			    </a>
			  </div>
			  <div class="media-body">
			  	<small class="galeria-categoria">galeria de fotos</small>
					<a href="{{ route('site.galeria.show', [str_slug($g->nome, '-'), $g->id]) }}"><p class="media-heading">{{ $g->nome }}</p></a>
					<small class="data-galeria">10/05/2017</small>
			  </div>
			</div>
			@endforeach
		</div>
		<div class="col-sm-4 col-xs-12 box oculto-xs">
			@foreach($galeriaSecun as $g)
			<div class="media-body">
				<a href="{{ route('site.galeria.show', [str_slug($g->nome, '-'), $g->id]) }}">
					<img src="{{ asset('uploads/galerias/pequena/'.$g->fotos[0]->nome) }}" class="img-responsive">
				</a>
				<small class="galeria-categoria">galeria de fotos</small>
				<a href="{{ route('site.galeria.show', [str_slug($g->nome, '-'), $g->id]) }}"><p>{{ $g->nome }}</p></a>
				<small class="data-galeria">10/05/2017</small>	
			</div>
			@endforeach
		</div>
	</div>
	<!-- FIM GALERIA DE FOTOS -->
@endsection