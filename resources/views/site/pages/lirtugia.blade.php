@extends('site.template')

@section('conteudo')
	<div class="page-lirtugia">
		<h2>LIRTUGIA DIÁRIA</h2>
		<hr>
		@php
			$ontem  = date('d/m/Y', strtotime('-1 days',strtotime($leituras['data'])));
			$amanha = date('d/m/Y', strtotime('+1 days',strtotime($leituras['data'])));
			$urlOntem = explode('/', $ontem);
			$urlAmanha = explode('/', $amanha);
		@endphp
		<div class="row navegacao-lirtugia">
			<a class="btn btn-default left" href='{{ route("site.lirtugia", [ $urlOntem[0],$urlOntem[1], $urlOntem[2]]) }}'><< Lirtugia de {{ $ontem }}</a>

			<a class="btn btn-default right" href='{{ route("site.lirtugia", [ $urlAmanha[0], $urlAmanha[1], $urlAmanha[2]]) }}'>Lirtugia de {{ $amanha }} >> </a>
		</div>

		<div class="col-sm-12 header">
			<h3>{{ $leituras['titulo_dia'] }}</h3>
			<span class="data">{{ date('d/m/Y', strtotime($leituras['data'])) }}</span>
			<span class="cor">{{ $leituras['cor'] }}</span>
		</div>
		@php 
			$i = 1;
		@endphp
		@foreach($leituras['leiturasDoDia'] as $key => $l)
		<div id="{{ $i }}"class="col-sm-12 leituras">
			<h3>{{ $key }}</h3>
			{!!  $l !!}
			@php
				$i++;
			@endphp
		</div>
		@endforeach
	</div>
@endsection

@section('js')


@endsection