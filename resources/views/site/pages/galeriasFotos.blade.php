@extends('site.template')

@section('conteudo')
	<div class="page-list">
		<h2>Galerias de Fotos</h2>
		<hr>
		@foreach($galerias as $n)
		<div class="row box">
			<div class="col-sm-4">
				<a href="{{ route('site.galeria.show', [str_slug($n->nome, '-'), $n->id]) }}">
					<img src="{{ asset('uploads/galerias/pequena/'.$n->fotos[0]->nome) }}" class="img-responsive">
				</a>
			</div>
			<a href="{{ route('site.galeria.show', [str_slug($n->nome, '-'), $n->id]) }}">
				<p>{{ $n->nome }}</p>
			</a>	
			{!! $n->descricao !!}
			<small class="noticia-data">10/05/2017</small>				
		</div>
		@endforeach
		<div class="paginate">
			{{ $galerias->links() }}
		</div>
	</div>
@endsection