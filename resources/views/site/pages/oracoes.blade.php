@extends('site.template')

@section('conteudo')
	<div class="page-list">
		<h2>Orações</h2>
		<hr>
		@foreach($oracoes as $n)
		<div class="row box">
			<div class="col-sm-4">
				<a href="{{ route('site.noticia.show', [ str_slug($n->titulo, '-') , $n->id ]) }}">
					<img src="{{ asset('uploads/noticias/pequena/'.$n->capa) }}" class="img-responsive">
				</a>
			</div>
			<small class="noticia-categoria">{{ $n->categoria->nome }}</small>
			<a href="{{ route('site.noticia.show', [ str_slug($n->titulo, '-') , $n->id ]) }}">
				<p>{{ $n->titulo }}</p>
			</a>	
			<small class="noticia-data">10/05/2017</small>				
		</div>
		@endforeach
		<div class="paginate">
			{{ $oracoes->links() }}
		</div>
	</div>
@endsection