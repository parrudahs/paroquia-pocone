@extends('site.template')

@section('conteudo')
	<div class="page-list">
		<h2>Comunidades</h2>
		<hr>
		@foreach($comunidades as $n)
		<div class="ol-sm-12">
			<a href="{{ route('site.pagina.show', [str_slug($n->titulo, '-'), $n->id]) }}">
				<p>{{ $n->titulo }}</p>
			</a>			
		</div>
		@endforeach
		<div class="paginate">
			{{ $comunidades->links() }}
		</div>
	</div>
@endsection