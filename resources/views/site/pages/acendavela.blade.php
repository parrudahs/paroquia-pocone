@extends('site.template')

@section('conteudo')
	<div class="oracao" >
		<ul class="nav nav-tabs" role="tablist">
		    <li role="presentation"><a href="#velas" aria-controls="velas" role="tab" data-toggle="tab">Velas Acesas</a></li>
		    <li role="presentation" class="active"><a href="#acenda" aria-controls="acenda" role="tab" data-toggle="tab">Acenda uma Vela</a></li>
	  	</ul>

	  	<div class="tab-content">
		    <div role="tabpanel" class="tab-pane" id="velas">
		    	<h2>Velas Acesas</h2><hr>
		    	@foreach($velas as $v)
		    		<div class="col-sm-4 text-center">
		    			<img src="{{ asset('imagens/vela.gif') }}" alt="{{ $v->nome }}">
		    			<p class="nome">{{ $v->nome }}</p>
		    		</div>
		    	@endforeach	
		    	<div class="paginate">
					{{ $velas->links() }}
				</div>
		    </div>

		    <div role="tabpanel" class="tab-pane active" id="acenda">		    	
				<h2>Acenda uma Vela</h2>
				<hr>
				<p>Preencha o formulário e acenda uma vela.</p>
				
				@if (count($errors) > 0)
			        <div class="alert alert-danger">
			            <ul>
			                @foreach ($errors->all() as $error)
			                    <li>{{ $error }}</li>
			                @endforeach
			            </ul>
			        </div>
			    @endif

			    @if(Session::has('success') > 0)
				    <div class="alert alert-success">
				            {{ Session::get('success') }}
				    </div>
				@endif

				<form method="post" action="{{ route('pedido.velas.store') }}">
					{{ csrf_field() }}
				    <div class="form-group">
				      <label for="disabledTextInput">Nome: </label>
				      <input type="text" name="nome" id="disabledTextInput" class="form-control" placeholder="Nome" value="{{ old('nome') }}">
				      <input type="hidden" name="status" value="0">
				    </div>
				    <div class="form-group">
				      <label for="disabledTextInput">Email: </label>
				      <input type="email" name="email" id="disabledTextInput" class="form-control" placeholder="Email" value="{{ old('email') }}" >
				    </div>
				    <div class="form-group">
				      <label for="disabledTextInput">Intenção: </label>
				      <textarea class="form-control" name="intencao" cols="30" rows="5">{{ old('mensagem') }}</textarea>
				    </div>
				    <div class="form-group">
				      <input type="checkbox" name="permitir" checked > Não permitir que minhas intenções sejam visualizadas por outras pessoas.
				    </div>
				    <input type="submit" class="btn btn-primary" name="envia" value="Enviar" >
				</form>
		    </div>



	  	</div>
	</div>
@endsection