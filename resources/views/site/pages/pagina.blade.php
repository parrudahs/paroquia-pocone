@extends('site.template')

@section('conteudo')
	<div class="page col-sm-12">
		<h2>{{ $pagina->titulo }}</h2>
		<hr>
		<div class="row">
			<!-- Go to www.addthis.com/dashboard to customize your tools --> 
			<div class="col-sm-9 addthis_inline_share_toolbox"></div>
			
			<div class="col-sm-3">
				<span class="date">{{ date('d/m/Y', strtotime($pagina->created_at)) }}</span>
			</div> 	
			
			
		</div>

		{!! $pagina->conteudo !!}
	</div>
@endsection