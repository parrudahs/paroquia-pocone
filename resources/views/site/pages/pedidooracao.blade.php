@extends('site.template')

@section('conteudo')
	<div class="oracao">
		<h2>Pedidos de Orações</h2>
		<hr>
		<p>Preencha o formulário e faça seu pedido de oração.</p>
		
		@if (count($errors) > 0)
	        <div class="alert alert-danger">
	            <ul>
	                @foreach ($errors->all() as $error)
	                    <li>{{ $error }}</li>
	                @endforeach
	            </ul>
	        </div>
	    @endif

	    @if(Session::has('success') > 0)
		    <div class="alert alert-success">
		            {{ Session::get('success') }}
		    </div>
		@endif

		<form method="post" action="{{ route('pedido.oracao.store') }}">
			{{ csrf_field() }}
		    <div class="form-group">
		      <label for="disabledTextInput">Nome: </label>
		      <input type="text" name="nome" id="disabledTextInput" class="form-control" placeholder="Nome" value="{{ old('nome') }}">
		      <input type="hidden" name="status" value="0">
		    </div>
		    <div class="form-group">
		      <label for="disabledTextInput">Telefone: </label>
		      <input type="text" name="telefone" value="{{ old('telefone') }}" id="disabledTextInput" class="form-control" placeholder="Telefone">
		    </div>
		    <div class="form-group">
		      <label for="disabledTextInput">Email: </label>
		      <input type="email" name="email" id="disabledTextInput" class="form-control" placeholder="Email" value="{{ old('email') }}" >
		    </div>
		    <div class="form-group">
		      <label for="disabledTextInput">Escreva seu pedido de oração: </label>
		      <textarea class="form-control" name="mensagem" cols="30" rows="5">{{ old('mensagem') }}</textarea>
		    </div>
		    <input type="submit" class="btn btn-primary" name="envia" value="Enviar" >
		    <input type="reset" class="btn btn-default" name="envia" value="Limpar Formulario" >
		</form>
	</div>
@endsection