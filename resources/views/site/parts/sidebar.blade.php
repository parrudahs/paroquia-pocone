@inject('pastorais', 'App\Models\Noticia')
<div class="col-sm-3">
	<div class="sidebar-direita">

		<div class="row busca">
			<div class="input-group">
			  <!--<input type="text" class="form-control" placeholder="Buscar">
		      <span class="input-group-btn">
		        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon glyphicon-search" aria-hidden="true"></span></button>
		      </span>-->
		    </div><!-- /input-group -->
		</div>

		<div class="row avisos-semana">
				</div>

		<div class="row">
			<h1>Liturgia Diária</h1>
			<ul class="nav nav-pills nav-stacked" id="lirtugia">		 
				
			</ul>
		</div>

		<div class="row">
			<h1>Pastorais, Movimentos e Ministerios</h1>
			<ul class="nav nav-pills nav-stacked">		
				@foreach($pastorais->where('tipo', 2)->where('ativo', 1)->where('categoria_id', 3)->get() as $p)		
					<li><a href="{{ route('site.pagina.show', [str_slug('Seja Dizimista', '-'), $p->id]) }}">{{ $p->titulo }}</a></li>
				@endforeach
			</ul>
		</div>
		<div class="row">
			<h2 class="fb"> Curta nossa página do facebook</h2>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.11';
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>

			<div class="fb-page" data-href="https://www.facebook.com/pnsrpocone/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/pnsrpocone/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pnsrpocone/">Paroquia Nossa Senhora do Rosario</a></blockquote></div>
			
		</div>
	</div>
</div>