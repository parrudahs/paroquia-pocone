<div class="row menu">
	<div class="sidebar-nav">
	  <div class="navbar navbar-default" role="navigation">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <span class="visible-xs navbar-brand">MENU</span>
	    </div>
	    <div class="navbar-collapse collapse sidebar-navbar-collapse">
	      <ul class="nav navbar-nav menu-destaque">
	        <li><a href="{{ route('site.inicio') }}" class="destaque">Página Inicial</a></li>
	      </ul>
	      <ul class="nav navbar-nav menu-destaque">
	       	<li class="destaque">Nossa Paroquia</li>
	        <li><a href="{{ route('site.pagina.show', [str_slug('História', '-'), 1]) }}">História</a></li>
	        
	        <li><a href="{{ route('site.comunidade.all') }}">Comunidades</a></li>

	        <li><a href="{{ route('site.pagina.show', [str_slug('Pároco', '-'),2]) }}">Pároco</a></li>
	        <li><a href="{{ route('site.pagina.show', [str_slug('Calendário Paroquial', '-'),3]) }}">Calendário Paroquial</a></li>
	        <li><a href="{{ route('site.pagina.show', [str_slug('Como chegar', '-'),4]) }}">Como chegar</a></li>
	        <li><a href="{{ route('site.pagina.show', [str_slug('Horários de missas', '-'),5]) }}">Horários de missas</a></li>
	        <li><a href="{{ route('site.pagina.show', [str_slug('Secretaria Paroquial', '-'),6]) }}">Secretaria Paroquial</a></li>
	      </ul>
	      <ul class="nav navbar-nav menu-destaque">
	       	<li class="destaque">Informações</li>
	        <li><a href="{{ route('site.noticia.all') }}">Notícias</a></li>
	        <li><a href="{{ route('site.artigo.all') }}">Artigos</a></li>
	        <li><a href="{{ route('site.oracao.all') }}">Oracões</a></li>
	        @php
				$data = explode('/', date('d/m/Y'));
			@endphp

	        <li><a href='{{ route("site.lirtugia", [ $data[0], $data[1], $data[2]]) }}'>Lirtugia Diária</a></li>
	      </ul>
	      <ul class="nav navbar-nav menu-destaque">
	        <li class="destaque">multimídia</li>
	        <li><a href="{{ route('site.galerias.all') }}">Galerias de Fotos</a></li>
	      </ul>
	    </div><!--/.nav-collapse -->		        
	  </div>
	</div>
</div>