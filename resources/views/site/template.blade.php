<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<title>Paroquia Nossa Senhora do Rosário - @yield('title')</title>
	<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/estilo.css') }}">
	@yield('css')
</head>
<body>
	<div class="container corpo">
		<!-- sidebar a esquerda -->
		<div class="row row-same-height">
			<div class="col-sm-3 col-xs-12 col-sm-height sidebar-esquerda"> 
				<div class="row logo">
					<a href="{{ route('site.inicio') }}" class="text-center">
						<img src="{{ asset('imagens/nova_logo_marca.png') }}" class="img-responsive">
					</a>
					<p class="text-center">Poconé, 
					@php 
						$dia = date('d');
						switch(date('m')) {
							case 1:
								$mes = "Janeiro";
								break;
							case 2:
								$mes = "Fevereiro";
								break;
							case 3:
								$mes = "Março";
								break;
							case 4:
								$mes = "Abril";
								break;
							case 5:
								$mes = "Maio";
								break;
							case 6:
								$mes = "Junho";
								break;
							case 7:
								$mes = "Julho";
								break;
							case 8:
								$mes = "Agosto";
								break;
							case 9:
								$mes = "Setembro";
								break;
							case 10:
								$mes = "Outubro";
								break;
							case 11:
								$mes = "Novembro";
								break;
							case 12:
								$mes = "Dezembro";
								break;
						}					
						$ano = date('Y');
					@endphp {{ $dia }}  de {{ $mes }} de {{ $ano }}</p>
				</div>
				@include('site.parts.menu')
			</div>	
			<!-- FIM sidebar a esquerda -->
			<!-- Conteudo -->
			<div class="col-sm-9 col-xs-12 col-sm-height all-content">
				<div class="row">
					<div class="col-sm-9 content">
						@yield('conteudo')
					</div>
					@include('site.parts.sidebar')
				</div>
			</div>
		</div>
		<!-- Fim Content -->
	<footer class="col-sm-12 rodape">
		<p class="text-center">Paróquia Nª Srª do Rosário &copy; Todos os direitos reservados</p>
	</footer>	
	</div>

	<!-- Scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript" ></script>
	<!-- Go to www.addthis.com/dashboard to customize your tools --> 
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5122481c589342a1"></script> 
	<script>
    ;(function($)
  	{
	    'use strict';
	    $(document).ready(function()
	    {
	      window.setTimeout(function() {
	          $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
	              $(this).remove(); 
	          });
	      }, 4000);


	      //var url = {{ route("api.liturgia", [date("d"), date("m"), date("Y")]) }};

	      //carregamento das leituras
	      $.ajax({
	      	url: '{{ route("api.liturgia", [date("d"), date("m"), date("Y")]) }}',
	      	type:'GET',
	      	success: function(data) {
	      		var leituras = Object.keys(data.leiturasDoDia);
	      		for(var i=0; i < leituras.length; i++){
	      			var htm = '<li><a href="{{ route("site.lirtugia", [date("d"), date("m"), date("Y")]) }}#'+ (i+1) +'">'+leituras[i]+'</a></li>';
	      			$('#lirtugia').append(htm);
	      			console.log(leituras[i]);
	      		}
	      	},
	      	error: function(data) {
	      		console.log(data);
	      	}
	      });

	    });
	})(window.jQuery);
	</script>
	@yield('js')

</body>
</html>		